package com.example.imadautocool;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TarifsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarifs);

        final Spinner spinnerFormules = (Spinner) findViewById(R.id.spinnerFormule);

        try {
            listeFormules(spinnerFormules);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Button btnConsulter = findViewById(R.id.btnConsult);
        btnConsulter.setOnClickListener(v -> {
            try {
                remplirTarifsHr();
                remplirTarifsKM();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    public void listeFormules(Spinner spinner1) throws IOException {

        OkHttpClient client = new OkHttpClient();
        ArrayList arrayListFormules = new ArrayList<String>();

        Request request = new Request.Builder()
                .url(getString(R.string.ipadresseFormules))
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {



            public void onResponse(Call call, Response response) throws IOException {

                String responseStr = response.body().string();
                JSONArray jsonArrayFormules = null;



                try {
                    jsonArrayFormules = new JSONArray(responseStr);

                    for (int i = 0; i < jsonArrayFormules.length(); i++) {
                        JSONObject jsonFormule = null;
                        jsonFormule = jsonArrayFormules.getJSONObject(i);
                        arrayListFormules.add(jsonFormule.getString("LIBELLEFORMULE"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                runOnUiThread(() -> { //on met le code a tourner en asyncrone , on le fait mouliner de coté, on a donc l'inteface mais les operations elles se font de tek

                    ArrayList<String> oui = new ArrayList<>();
                    ArrayAdapter<String> arrayAdapterFormules = new ArrayAdapter<String>(TarifsActivity.this, android.R.layout.simple_spinner_item, arrayListFormules);

                    spinner1.setAdapter(arrayAdapterFormules);

                    });
                };

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }


        });
    }







    public void remplirTarifsHr() throws IOException {

        OkHttpClient client = new OkHttpClient();
        ArrayList arrayTarifHr = new ArrayList<String>();
        final Spinner libelleFormule = findViewById(R.id.spinnerFormule);

        RequestBody formBody = new FormBody.Builder()
                .add("libelleFormule", libelleFormule.getSelectedItem().toString())
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.ipadresseTarifHr))
                .post(formBody)
                .build();


        Call call = client.newCall(request);

        call.enqueue(new Callback() {

            public void onResponse(Call call, Response response) throws IOException {
                String responseStr = response.body().string();
                JSONArray jsonArrayTarifsHr = null;


                Log.d("", responseStr+" Reponse du boutton valider quand on choisis la categorie");
                try {
                    jsonArrayTarifsHr = new JSONArray(responseStr);
                    for (int i = 0; i < jsonArrayTarifsHr.length(); i++) {
                        JSONObject jsonTarifsHr = null;
                        jsonTarifsHr = jsonArrayTarifsHr.getJSONObject(i);
                        arrayTarifHr.add("Vehicule de catégorie : " + jsonTarifsHr.getString("CODECATEG") + " " +
                                jsonTarifsHr.getString("TARIFH") + "€" + " par " +
                                jsonTarifsHr.getString("CODETRANCHEH"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                runOnUiThread(() -> { //on met le code a tourner en asyncrone , on le fait mouliner de coté, on a donc l'inteface mais les operations elles se font de tek

                    ListView listviewV = (ListView) findViewById(R.id.lvTarifHr);

                    ArrayAdapter<String> arrayAdapterClasses = new ArrayAdapter<String>(TarifsActivity.this, android.R.layout.simple_list_item_1, arrayTarifHr);

                    listviewV.setAdapter(arrayAdapterClasses);




                });

            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }
        });
    }


    public void remplirTarifsKM() throws IOException {

        OkHttpClient client = new OkHttpClient();
        ArrayList arrayTarifKM = new ArrayList<String>();
        final Spinner libelleFormule = findViewById(R.id.spinnerFormule);

        RequestBody formBody = new FormBody.Builder()
                .add("libelleFormule", libelleFormule.getSelectedItem().toString())
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.ipadresseTarifKm))
                .post(formBody)
                .build();


        Call call = client.newCall(request);

        call.enqueue(new Callback() {

            public void onResponse(Call call, Response response) throws IOException {
                String responseStr = response.body().string();
                JSONArray jsonArrayTarifsKM = null;


                Log.d("", responseStr+" Reponse du boutton valider quand on choisis la categorie");
                try {
                    jsonArrayTarifsKM = new JSONArray(responseStr);
                    for (int i = 0; i < jsonArrayTarifsKM.length(); i++) {
                        JSONObject jsonTarifsKM = null;
                        jsonTarifsKM = jsonArrayTarifsKM.getJSONObject(i);
                        arrayTarifKM.add("Vehicule de catégorie : " + jsonTarifsKM.getString("CODECATEG") + " " +
                                jsonTarifsKM.getString("CODETRANCHEKM") + " " +
                                jsonTarifsKM.getString("TARIFKM") + " € /KM ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                runOnUiThread(() -> { //on met le code a tourner en asyncrone , on le fait mouliner de coté, on a donc l'inteface mais les operations elles se font de tek

                    ListView listviewV = (ListView) findViewById(R.id.lvTarifKM);

                    ArrayAdapter<String> arrayAdapterClasses = new ArrayAdapter<String>(TarifsActivity.this, android.R.layout.simple_list_item_1, arrayTarifKM);

                    listviewV.setAdapter(arrayAdapterClasses);




                });

            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }
        });
    }



}